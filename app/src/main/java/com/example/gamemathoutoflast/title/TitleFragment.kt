package com.example.gamemathoutoflast.title

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamemathoutoflast.*
import com.example.gamemathoutoflast.databinding.FragmentTitleBinding

class TitleFragment : Fragment() {
    private lateinit var binding: FragmentTitleBinding
    private lateinit var viewModel: TitleViewModel
    private lateinit var viewModelFactory: TitleViewModelFactory
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentTitleBinding>(
            inflater, R.layout.fragment_title, container, false
        )
        viewModelFactory = TitleViewModelFactory(
            finalCorrect = TitleFragmentArgs.fromBundle(requireArguments()).numCorrect,
            finalIncorrect = TitleFragmentArgs.fromBundle(requireArguments()).numIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(TitleViewModel::class.java)
        viewModel.correct.observe(viewLifecycleOwner, Observer { newCoerrect ->
            binding.countCorrect.text = newCoerrect.toString()
        })
        viewModel.incorrect.observe(viewLifecycleOwner, Observer { newIncoerrect ->
            binding.countIncorrect.text = newIncoerrect.toString()
        })
        binding.btnPlus.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToPlusFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        binding.btnMinus.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToMinusFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        binding.btnMultiplied.setOnClickListener { view: View ->
            view.findNavController().navigate(
                TitleFragmentDirections.actionTitleFragmentToMutipliedFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }
    }