package com.example.gamemathoutoflast.title

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TitleViewModel(finalCorrect: Int, finalIncorrect: Int) : ViewModel() {
    //Correct
    private var _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct

    //Incorrect
    private var _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    init {
        _correct.value = finalCorrect
        _incorrect.value = finalIncorrect

    }

}