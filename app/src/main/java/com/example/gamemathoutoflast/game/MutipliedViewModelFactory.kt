package com.example.gamemathoutoflast.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MutipliedViewModelFactory(private val totalCorrect: Int, private val totalIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MutipliedViewModel::class.java)) {
            return MutipliedViewModel(totalCorrect, totalIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}