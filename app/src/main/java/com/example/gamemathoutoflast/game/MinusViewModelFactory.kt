package com.example.gamemathoutoflast.game

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class MinusViewModelFactory(private val totalCorrect: Int, private val totalIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(MinusViewModel::class.java)) {
            return MinusViewModel(totalCorrect, totalIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}