package com.example.gamemathoutoflast.game

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import kotlin.random.Random

class MinusViewModel(totalCorrect: Int, totalIncorrect: Int) : ViewModel() {
    private val _correct = MutableLiveData<Int>()
    val correct: LiveData<Int>
        get() = _correct
    private val _incorrect = MutableLiveData<Int>()
    val incorrect: LiveData<Int>
        get() = _incorrect
    private val _randomNumber1 = MutableLiveData<Int>()
    val randomNumber1 : LiveData<Int>
        get()= _randomNumber1
    private val _randomNumber2 = MutableLiveData<Int>()
    val randomNumber2 : LiveData<Int>
        get() = _randomNumber2
    private val _ansNumber = MutableLiveData<Int>()
    val ansNumber : LiveData<Int>
        get() = _ansNumber
    private val _position = MutableLiveData<Int>()
    val position : LiveData<Int>
        get() = _position
    private val _ansPosition1 = MutableLiveData<Int>()
    val ansPosition1 : LiveData<Int>
        get() = _ansPosition1
    private val _ansPosition2 = MutableLiveData<Int>()
    val ansPosition2 : LiveData<Int>
        get() = _ansPosition2
    private val _ansPosition3 = MutableLiveData<Int>()
    val ansPosition3 : LiveData<Int>
        get() = _ansPosition3
    init {
        _correct.value = totalCorrect
        _incorrect.value = totalIncorrect
        _randomNumber1.value = 0
        _randomNumber2.value = 0
        _ansNumber.value = 0
        _position.value = 0
        _ansPosition1.value = 0
        _ansPosition2.value = 0
        _ansPosition3.value = 0
        randomGen()
    }
    fun resultCorrect() {
        _correct.value = _correct.value?.plus(1)
    }
    fun resultIncorrect(){
        _incorrect.value = _incorrect.value?.plus(1)
    }
    fun randomGen(){
        _randomNumber1.value = Random.nextInt(1,9)
        _randomNumber2.value = Random.nextInt(1,9)
        _ansNumber.value = _randomNumber1.value!! - _randomNumber2.value!!
        _position.value = Random.nextInt(1,3)
        _ansPosition1.value = _ansNumber.value?.plus(1)
        _ansPosition2.value = _ansNumber.value
        _ansPosition3.value = _ansNumber.value?.minus(1)
    }
}