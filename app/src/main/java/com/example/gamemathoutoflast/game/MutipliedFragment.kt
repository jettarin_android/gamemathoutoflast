package com.example.gamemathoutoflast.game

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.findNavController
import com.example.gamemathoutoflast.R
import com.example.gamemathoutoflast.title.TitleViewModel
import com.example.gamemathoutoflast.databinding.FragmentMutipliedBinding

import kotlin.random.Random

class MutipliedFragment : Fragment() {
    private lateinit var binding: FragmentMutipliedBinding
    private lateinit var viewModel: MutipliedViewModel
    private lateinit var viewModelFactory: MutipliedViewModelFactory

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate<FragmentMutipliedBinding>(
            inflater, R.layout.fragment_mutiplied,container,false
        )
        viewModelFactory = MutipliedViewModelFactory(
            totalCorrect = MutipliedFragmentArgs.fromBundle(requireArguments()).numCorrect,
            totalIncorrect = MutipliedFragmentArgs.fromBundle(requireArguments()).numIncorrect
        )
        viewModel = ViewModelProvider(this, viewModelFactory).get(MutipliedViewModel::class.java)
        val a  = 0
        val b = 0
        val correct= viewModel.correct.value
        val incorrect = viewModel.incorrect.value
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            Startgame(a, b, correct, incorrect)
        }
        binding.lifecycleOwner = viewLifecycleOwner
        return binding.root
    }

    @SuppressLint("SetTextI18n")
    @RequiresApi(Build.VERSION_CODES.O)
    fun Startgame(a: Int, b: Int, correct: Int?, incorrect: Int?) {
        viewModel.randomGen()
        val num1 = viewModel.randomNumber1.value
        val num2 = viewModel.randomNumber2.value
        val result = viewModel.ansNumber.value
        val txtNum1 = binding.txtRandomNum1
        txtNum1.text = "$num1"
        val txtNum2 = binding.txtRandomNum2
        txtNum2.text = "$num2"
        val btn1 = binding.btnResult1
        val btn2 = binding.btnResult2
        val btn3= binding.btnResult3
        var countCorrect = a
        var countIncorrect = b
        val countCorrectText: TextView = binding.countCorrect
        val countIncorrectText: TextView = binding.countIncorrect
        val txt3 : TextView = binding.TV3
        val position = viewModel.position.value
        fun showIncorrect(){
            txt3.visibility = View.VISIBLE
            txt3.text = "Incorrect"
            txt3.setTextColor(Integer.parseUnsignedInt("ffff0000",16))
            countIncorrect++
            countCorrectText.text = "$countCorrect"
            countIncorrectText.text = "$countIncorrect"
        }
        fun showCorrect(){
            txt3.visibility = View.VISIBLE
            txt3.text = "Correct"
            txt3.setTextColor(Integer.parseUnsignedInt("ff008000",16))
            countCorrect++
            countCorrectText.text = "$countCorrect"
            countIncorrectText.text = "$countIncorrect"
            Startgame(countCorrect, countIncorrect,correct,incorrect)
        }
        fun checkGame(text: Button, result : Int){
            binding.apply {
                if (text.text == result.toString()) {
                    viewModel.resultCorrect()
                    showCorrect()
                }
                else {
                    viewModel.resultIncorrect()
                    showIncorrect()
                }
            }
        }
        if (position==1){
            btn1.text = viewModel.ansPosition1.value.toString()
            btn2.text = viewModel.ansPosition2.value.toString()
            btn3.text = viewModel.ansPosition3.value.toString()
        }
        if (position==2) {
            btn1.text = viewModel.ansPosition2.value.toString()
            btn2.text = viewModel.ansPosition3.value.toString()
            btn3.text = viewModel.ansPosition1.value.toString()
        }
        if (position==3) {
            btn1.text = viewModel.ansPosition3.value.toString()
            btn2.text = viewModel.ansPosition1.value.toString()
            btn3.text = viewModel.ansPosition2.value.toString()
        }
        btn1.setOnClickListener {
            if (result != null) {
                checkGame(btn1,result)
            }
        }
        btn2.setOnClickListener{
            if (result != null) {
                checkGame(btn2,result)
            }
        }
        btn3.setOnClickListener{
            if (result != null) {
                checkGame(btn3,result)
            }
        }
        binding.btnSave.setOnClickListener { view: View ->

            view.findNavController().navigate(
                MutipliedFragmentDirections.actionMutipliedFragmentToTitleFragment(
                    viewModel.correct.value!!,
                    viewModel.incorrect.value!!
                )
            )
        }
    }

}