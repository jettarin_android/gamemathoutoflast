package com.example.gamemathoutoflast.game


import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class PlusViewModelFactory(private val totalCorrect: Int, private val totalIncorrect: Int) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(PlusViewModel::class.java)) {
            return PlusViewModel(totalCorrect, totalIncorrect) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}
